
const express = require("express");
const cors = require("cors")
const app = express();
app.use(cors());
const rootRoute = require("./src/rootRoute");

app.use(express.json());
app.use("/api",rootRoute);

const PORT = 8080;
app.listen(PORT);