const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('hr_user', {
    user_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    user_username: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    user_password: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    user_type: {
      type: DataTypes.STRING(30),
      allowNull: true
    },
    user_fullname: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    user_phone: {
      type: DataTypes.STRING(20),
      allowNull: true
    },
    user_status: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    user_createdate: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_delete: {
      type: DataTypes.TINYINT,
      allowNull: true
    },
    chucvu_id: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    nhan_vien_bc_giao: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    nhan_vien_bc_nhan: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    phong_ban: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    quan_ly_giao_viec: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    quan_ly_hop_dong: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    quan_ly_thanh_toan: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    tim_kiem_lich: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    xem_bc_cong_no: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    xem_bc_cong_viec: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    xem_bc_doanh_thuc: {
      type: DataTypes.TINYINT,
      allowNull: false
    },
    danhmuc_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    }
  }, {
    sequelize,
    tableName: 'hr_user',
    timestamps: false,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "user_id" },
        ]
      },
    ]
  });
};
