var DataTypes = require("sequelize").DataTypes;
var _hr_user = require("./hr_user");
var _tasks = require("./tasks");

function initModels(sequelize) {
  var hr_user = _hr_user(sequelize, DataTypes);
  var tasks = _tasks(sequelize, DataTypes);


  return {
    hr_user,
    tasks,
  };
}
module.exports = initModels;
module.exports.initModels = initModels;
module.exports.default = initModels;
