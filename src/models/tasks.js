const Sequelize = require('sequelize');
module.exports = function(sequelize, DataTypes) {
  return sequelize.define('tasks', {
    task_id: {
      autoIncrement: true,
      type: DataTypes.INTEGER,
      allowNull: false,
      primaryKey: true
    },
    department_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    contract_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    task_user: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    task_name: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    task_start: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    task_finish: {
      type: DataTypes.DATEONLY,
      allowNull: true
    },
    task_note: {
      type: DataTypes.STRING(255),
      allowNull: true
    },
    task_done: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    task_date_done: {
      type: DataTypes.DATE,
      allowNull: true
    },
    user_created: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    task_parent_id: {
      type: DataTypes.INTEGER,
      allowNull: true
    },
    deleted_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    created_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    updated_at: {
      type: DataTypes.DATE,
      allowNull: true
    },
    task_status: {
      type: DataTypes.INTEGER,
      allowNull: false
    }
  }, {
    sequelize,
    tableName: 'tasks',
    timestamps: false,
    paranoid: true,
    indexes: [
      {
        name: "PRIMARY",
        unique: true,
        using: "BTREE",
        fields: [
          { name: "task_id" },
        ]
      },
    ]
  });
};
