const express = require("express");
const { getTask, createTask, updateTask } = require("./taskService");
const {checkToken} = require("../auth/auth");

const taskRoute = express.Router();

taskRoute.get("/getTask",getTask);
taskRoute.post("/createTask",checkToken,createTask);
taskRoute.post("/updateTask",checkToken,updateTask);

module.exports = taskRoute;