const hr_user = require('../models/hr_user');
const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const moment = require("moment")
const model = initModels(sequelize);

let getTask = async (req,res) => {
    try{
        let data = await model.tasks.findAll({
            where:{
                task_status: 1
            }
        });

        if(data == null){
        res.status(400).send("Không tìm thấy công việc!");
    }else{
        res.status(200).send({
            message: "Lấy công việc thành công",
            data
        })
    }
    }catch(err){
        res.status(500).send(err);
    }
    
}
let createTask = async (req,res) => {
    let user = req.user.data;
    let body = req.body;
    body.user_created = user.user_id;
    // lấy thời gian hiện tại
    let today = new Date();
    let date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+ today.getDate();
    let time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
    body.created_at = date + " " + time;
    console.log("ngay_gio:",date+time);
    // 
    // console.log(body);
    let data = await model.tasks.create(body);
    res.status(201).send({
        message: "Tạo Công Việc Thành công",
        data
    })
}
let updateTask = async (req,res) => {
    try{
        let body = req.body;
        let now = moment().format("YYYY-MM-DD hh:mm:ss");
        body.updated_at = now;
        await model.tasks.update(body,{
            where:{
                task_id: body.task_id
            }
        });
        let data = await model.tasks.findOne({
            where:{
                task_id: body.task_id
            }
        })
        res.status(200).send(data);
    }catch(err){
        res.status(500).send(err);
    }
}
module.exports = {getTask,createTask,updateTask};