const express = require("express");
const { login, userList } = require("./userService");
const userRoute = express.Router();

userRoute.post("/login",login);
userRoute.get("/userList",userList);

module.exports = userRoute;