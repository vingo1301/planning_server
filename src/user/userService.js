const {createToken} = require('../auth/auth');
const sequelize = require('../models/index');
const initModels = require('../models/init-models');
const model = initModels(sequelize);

let login = async (req,res) => {
    // try{
        let {username,password} = req.body;
        let data = await model.hr_user.findOne({
        where:{
            user_username: username
            }
        });
        if(data == null){
            res.status(400).send({
                message: "Không tìm thấy tài khoản!"
             });
        }else{
            if(data.user_password == password){
                if(data.user_status == 2){
                    res.status(400).send({
                        message: "Tài khoản đang bị khoá!"
                    });
                }else{
                    data.user_password = "";
                    let token = await createToken({data});
                    res.status(200).send({
                        message: "Đăng nhập thành công",
                        data,
                        token
                        })
                    }
            }else{
                res.status(400).send({
                    message: "Mật khẩu không chính xác"
                })
            }
        }
    // }catch(err){
    //     res.status(500).send(err);
    // }
}
let userList = async(req,res) => {
    try{
        let data = await model.hr_user.findAll({
            attributes: ["user_id","user_username","user_fullname"]
        });
        res.status(200).send({
            message: "Lấy danh sach thành công!",
            data
        })
    }catch(err){
        res.status(500).send(err);
    }
    
    
}
module.exports = {login,userList}