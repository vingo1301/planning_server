const express = require("express");
const userRoute = require("./user/userController");
const taskRoute = require("./task/taskController");
const rootRoute = express.Router();

rootRoute.use("/user",userRoute);
rootRoute.use("/task",taskRoute);

module.exports = rootRoute;